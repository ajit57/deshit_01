
<?php
include 'header.php';
include '../vendor/autoload.php';
use App\Student\Student;
$obj = new Student;
$obj->setData($_GET);
$data = $obj->edit();
$hobbies = explode(',', $data->hobbies);
?>
<section class="main-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h2>Student List <a class="btn btn-primary pull-right" href="index.php">Back</a></h2>
				<form action="update.php" method="POST">
					<input type="hidden" name="id" value="<?php echo $data->id; ?>">
					<div class="form-group">
						<label for="name">Name :</label>
						<input type="text" class="form-control" name="name" value="<?php echo $data->name; ?>" id="name">
					</div>
					<div class="form-group">
						<label for="email">Email : </label>
						<input type="email" class="form-control" value="<?php echo $data->email; ?>" name="email" id="email">
					</div>
					<div class="form-group">
						<label for="birth_date">Date of Birth : </label>
						<input type="date" class="form-control" name='birth_date' value="<?php echo $data->birth_date; ?>" id="birth_date">
					</div>
					<div class="form-group">
						<label for="gender">Select a Gender : </label>
						<input type="radio" class="radio-inline" value="male" name="gender" <?php if($data->gender == 'male') echo 'checked'; ?>> Male
						<input type="radio" class="radio-inline" value="female" name="gender" <?php if($data->gender == 'female') echo 'checked'; ?>> Female
					</div>
					<div class="form-group">
						<label for="hobbies">Select your Hobby : </label>
						<input type="checkbox" name="hobbies[]" value="swimming" <?php if(in_array('swimming', $hobbies)) echo "checked"; ?>> Swimming
						<input type="checkbox" name="hobbies[]" value="gardening"<?php if(in_array('gardening', $hobbies)) echo "checked"; ?>> Gardening
						<input type="checkbox" name="hobbies[]" value="traveling" <?php if(in_array('traveling', $hobbies)) echo "checked"; ?>> Traveling
						<input type="checkbox" name="hobbies[]" value="fishing" <?php if(in_array('fishing', $hobbies)) echo "checked"; ?>> Fishing
					</div>
					<div class="form-group">
						<label for="address">Address : </label>
						<textarea name="address" id="address" cols="30" rows="10" class="form-control"><?php echo $data->address; ?></textarea>
					</div>
					<button type="submit">Update</button>
				</form>
			</div>
		</div>
	</div>
</section>

<?php
include 'footer.php';
?>
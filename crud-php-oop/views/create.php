
<?php
include 'header.php';
?>
<section class="main-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2">
				<h2>Student List <a class="btn btn-primary pull-right" href="index.php">Back</a></h2>
				<form action="store.php" method="POST">
					<div class="form-group">
						<label for="name">Name :</label>
						<input type="text" class="form-control" name="name" id="name">
					</div>
					<div class="form-group">
						<label for="email">Email : </label>
						<input type="email" class="form-control" name="email" id="email">
					</div>
					<div class="form-group">
						<label for="birth_date">Date of Birth : </label>
						<input type="date" class="form-control" name='birth_date' id="birth_date">
					</div>
					<div class="form-group">
						<label for="gender">Select a Gender : </label>
						<input type="radio" class="radio-inline" value="male" name="gender" checked> Male
						<input type="radio" class="radio-inline" value="female" name="gender"> Female
					</div>
					<div class="form-group">
						<label for="hobbies">Select your Hobby : </label>
						<input type="checkbox" name="hobbies[]" value="swimming"> Swimming
						<input type="checkbox" name="hobbies[]" value="gardening"> Gardening
						<input type="checkbox" name="hobbies[]" value="traveling"> Traveling
						<input type="checkbox" name="hobbies[]" value="fishing"> Fishing
					</div>
					<div class="form-group">
						<label for="address">Address : </label>
						<textarea name="address" id="address" cols="30" rows="10" class="form-control"></textarea>
					</div>
					<button type="submit">Create</button>
				</form>
			</div>
		</div>
	</div>
</section>

<?php
include 'footer.php';
?>
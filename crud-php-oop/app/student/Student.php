<?php
namespace App\Student;
use App\Database\Database;
use PDO;
class Student extends Database
{
	protected $id;
	protected $name;
	protected $email;
	protected $birth_date;
	protected $gender;
	protected $hobbies;
	protected $address;
	public function setData($request)
	{
		if(array_key_exists('id', $request))
			$this->id = $request['id'];
		if(array_key_exists('name', $request))
			$this->name = $request['name'];
		if(array_key_exists('email', $request))
			$this->email = $request['email'];
		if(array_key_exists('birth_date', $request))
			$this->birth_date = $request['birth_date'];
		if(array_key_exists('gender', $request))
			$this->gender = $request['gender'];
		if(array_key_exists('hobbies', $request))
			$this->hobbies = $request['hobbies'];
		if(array_key_exists('address', $request))
			$this->address = $request['address'];
	}

	public function store()
	{
		$query = "INSERT into students (name, email, birth_date, gender, hobbies, address) Values(?, ?, ?, ?, ?, ?)";
		$sth = $this->db->prepare($query);
		$data = array($this->name, $this->email, $this->birth_date, $this->gender, $this->hobbies, $this->address);
		$status = $sth->execute($data);
		if($status){
			echo "<script>alert('Student Created Successfully!');location.href='index.php'</script>";
		}else{
			echo "<script>alert('Student is not Created!');location.href='index.php'</script>";
		}
	} 

	public function index()
	{
		$query = "SELECT * from students";
		$sth = $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$alldata = $sth->fetchAll();
		return $alldata;
	}

	public function edit()
	{
		$query = "SELECT * from students WHERE id=".$this->id;
		$sth = $this->db->query($query);
		$sth->setFetchMode(PDO::FETCH_OBJ);
		$data = $sth->fetch();
		return $data;
	}

	public function update()
	{
		$a = $this->id;
		//var_dump($a);
		$query = "UPDATE students SET name=?, email=?, birth_date=?, gender=?, hobbies=?, address=? WHERE id=".$a;
		$sth = $this->db->prepare($query);
		$data = [$this->name, $this->email, $this->birth_date, $this->gender, $this->hobbies, $this->address];
		$status = $sth->execute($data);
		if($status){
			echo "<script>alert('Student Updated Successfully!');location.href='index.php'</script>";
		}else{
			echo "<script>alert('Student is not Update!');location.href='index.php'</script>";
		}
	}

	public function delete()
	{
		$query = "DELETE from students WHERE id=".$this->id;
		$status = $this->db->query($query);
		if($status){
			echo "<script>alert('Student Deleted Successfully!');location.href='index.php'</script>";
		}else{
			echo "<script>alert('Student is not Deleted!');location.href='index.php'</script>";
		}
	}

}
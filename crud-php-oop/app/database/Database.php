<?php
namespace App\Database;
use PDO, PDOException;

class Database
{
    protected $db;
    protected $host = 'localhost';
    protected $dbname = 'crud-data-import';
    protected $dbusername = 'root';
    protected $dbuserpass = "";
    public function __construct()
    {
        try {
            $this->db = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->dbusername, $this->dbuserpass);
            
            //echo "Database connected...";

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
}
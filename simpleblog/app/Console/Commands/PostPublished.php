<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;
use App\Mail\PostCreated;

class PostPublished extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:postpublished';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make a postpublished Mail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*$totalposts = DB::table('posts')->count('title');
        echo $this->info($totalposts);*/
        \Mail::to('ajit@gmail.com')->send(new PostCreated('Some User'));
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddCategory extends Model
{
    protected $fillable = ['name', 'slug', 'description'];
}

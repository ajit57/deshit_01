<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*$ip = $request->ip();
        if($ip == "127.0.0.1"){
            return redirect('/');
            
        }*/
        $id = Auth::user()->id;
        if($id == 1){
            
            return $next($request);
        }
        return redirect("/");
        
    }
}

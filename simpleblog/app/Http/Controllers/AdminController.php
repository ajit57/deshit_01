<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
	/*public function __construct()
	{
		$this->middleware('checkadmin');
	}*/
    public function index()
    {
    	return view('admin');
    }

    public function create()
    {
    	return view('create');
    }
}

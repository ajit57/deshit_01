<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AddCategory;

class AddCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
    	$addcategorys = AddCategory::all();
    	return view('adds.addscategory')->with('addcategorys', $addcategorys);
    }


    public function store(Request $request)
    {
    	$addscategory = new AddCategory;
    	$addscategory->name = $request->name;
    	$addscategory->slug = $request->slug;
    	$addscategory->description = $request->description;
    	$addscategory->save();
    	return redirect('addcategories');
    }
}

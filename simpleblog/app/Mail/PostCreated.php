<?php

namespace App\Mail;

use App\Post;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostCreated extends Mailable
{
    use Queueable, SerializesModels;


    public $posttitle;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($posttitle)
    {
        $this->posttitle = $posttitle;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //$posts = Post::all();
        return $this->view('emails.postcreate')
        ->with(['posttitle'=>$this->posttitle]);
    }
}

@extends('layouts.app');

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1>Addvertisement Categories <a href="#" class="btn btn-success pull-right" data-toggle="modal" data-target="#myModal">Create</a></h1>
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <h4 class="modal-title" id="myModalLabel">Create AddCategories</h4>
				      </div>
				      <div class="modal-body">
				        <form action="{{route('addcategories.store')}}" method="POST">
				        	{{csrf_field()}}
				        	<div class="form-group">
				        		<label for="name">Name</label>
				        		<input type="text" name="name" id="name" class="form-control">
				        	</div>
				        	<div class="form-group">
				        		<label for="slug">Slug</label>
				        		<input type="text" name="slug" id="slug" class="form-control">
				        	</div>
				        	<div class="form-group">
				        		<label for="description">Description</label>
				        		<textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
				        	</div>
				        	<button class="btn btn-primary">Create</button>
				        </form>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="post-categories">
					<div class="btn-group" role="group" aria-label="...">

						@foreach($addcategorys as $addcategory)
					  <button type="button" class="btn btn-default">{{$addcategory->name}}</button>
					  
					  @endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
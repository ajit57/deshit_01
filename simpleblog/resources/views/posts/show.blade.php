{{$post->name}}
@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1>Post Details : <a href="{{route('posts.index')}}" class="btn btn-primary pull-right">Back</a></h1>
				<h3>Post Title : {{$post->title}}</h3>
				<p>Description: {{$post->description}} </p>
			</div>
		</div>
	</div>
@endsection
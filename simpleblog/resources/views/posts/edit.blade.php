
@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1>Edit posts <a href="{{route('posts.index')}}" class="btn btn-primary pull-right">Back</a></h1>
				@if(count($errors->all())>0)
				<div class="alert alert-danger">
					<ul>
						
					@foreach($errors->all() as $error)
						<li>{{$error}}</li>
					@endforeach
					</ul>
				</div>
				@endif
				<form action="{{route('posts.update', $post->id)}}" method="POST" enctype="multipart/form-data">
					{{method_field('PUT')}}
					{{csrf_field()}}
					<div class="form-group">
						<label for="title">Title</label>
						<input type="text" value="{{$post->title}}" name="title" id="title" class="form-control" >
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<label for="image">Select a thumbnail</label>
								<input type="file" name="image" id="image" >
							</div>
							<div class="form-group">
								<label for="post_category_id">Select a Category</label>
								<select name="post_category_id" id="post_category_id">
									@foreach($postcategorys as $postcategory)
									<option value="{{$postcategory->id}}" @if($postcategory->id == $post->post_category_id ) Selected @endif>{{$postcategory->name}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-sm-6">
							<img src="{{asset($post->image)}}" alt="" class="img-responsive" >
						</div>
					</div>
					
					<div class="form-group">
						<label for="description">Description</label>
						<textarea name="description" id="description" cols="30" rows="10" class="form-control">{{$post->description}}</textarea>
					</div>
					<button class="btn btn-success">Update</button>
				</form>
			</div>
		</div>
	</div>
@endsection
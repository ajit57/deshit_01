@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1>All posts <a href="{{route('posts.create')}}" class="btn btn-primary pull-right">Create Post</a></h1>
				@if (session('status'))
				    <div class="alert alert-success">
				        {{ session('status') }}
				    </div>
				@endif
				<table class="display table table-bordered" id="myTable" cellspacing="0" width="100%">
					<thead>
						<tr>
							<td>Sl</td>
							<td>Title</td>
							<!-- <td>Description</td> -->
							<td>Created By</td>
							<td>Category</td>
							<td>Image</td>
							<td>Date</td>
							<td width="150"></td>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<td>Sl</td>
							<td>Title</td>
							<!-- <td>Description</td> -->
							<td>Created By</td>
							<td>Category</td>
							<td>Image</td>
							<td>Date</td>
							<td width="150"></td>
						</tr>
					</tfoot>
					<tbody>
						@foreach($posts as $post)
						<tr>
							<td>{{$post->id}}</td>
							<td>{{$post->title}}</td>
							<!-- <td class="post-desc">{{$post->description}}</td> -->
							<td>{{$post->user->name}}</td>
							<td>{{$post->post_category->name}}</td>
							<td><img src="{{$post->image}}" alt="" class="img-responsive"></td>
							<td>{{$post->created_at->toFormattedDateString()}}</td>
							<td>
								<a class="btn btn-primary btn-sm" href="{{route('posts.show', $post->id)}}">View</a>
								<a class="btn btn-warning btn-sm" href="{{route('posts.edit', $post->id)}}">Edit</a>
								<a href="#" onclick="return confirm('are you sure?')">
									<form action="{{route('posts.destroy', $post->id)}}" method="POST">
										{{csrf_field()}}
										{{method_field('DELETE')}}
										<input type="submit" class="btn btn-danger btn-sm" value="Delete">
									</form>
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script type="text/javascript">
		$(document).ready(function(){
		    $('#myTable').DataTable();
		});
	</script>
@endsection
@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h1>Create posts <a href="{{route('posts.index')}}" class="btn btn-primary pull-right">Back</a></h1>
				@if(count($errors->all())>0)
				<div class="alert alert-danger">
					<ul>
						
					@foreach($errors->all() as $error)
						<li>{{$error}}</li>
					@endforeach
					</ul>
				</div>
				@endif
				<form action="{{route('posts.store')}}" method="POST" enctype="multipart/form-data">
					{{csrf_field()}}
					<div class="form-group">
						<label for="title">Title</label>
						<input type="text" name="title" id="title" class="form-control"> 
					</div>
					<div class="form-group">
						<label for="image">Select a thumbnail</label>
						<input type="file" name="image" id="image">
					</div>
					<div class="form-group">
						<label for="post_category_id">Select a Category</label>
						<select name="post_category_id" id="post_category_id">
							@foreach($postcategorys as $postcategory)
							<option value="{{$postcategory->id}}">{{$postcategory->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="description">Description</label>
						<textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
					</div>
					<button class="btn btn-success">Create</button>
				</form>
				
			</div>
		</div>
	</div>
@endsection
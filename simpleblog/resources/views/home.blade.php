@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="well">
                        Total Posts : {{$total_post}}
                    </div>
                    <div class="well">
                        Total Users : {{DB::table('users')->count()}}
                    </div>
                    <div class="well">
                        Total Post Categories : {{DB::table('post_categories')->count()}}
                    </div>
                    <div class="well"> Current Date : {{\Carbon\Carbon::now()}}</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

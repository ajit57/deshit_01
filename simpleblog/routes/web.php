<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware'=>['checkadmin']], function(){
	Route::get('/admin', 'AdminController@index');
	Route::get('/create', 'AdminController@create');
});





Route::get('/single', function(){
	return view('single');
})->name('sin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('/posts', 'PostController');
Route::resource('/postcategory', 'PostCategoryController');

Route::get('/addcategories', 'AddCategoryController@index')->name('addcategories');
Route::post('/addcategories/store', 'AddCategoryController@store')->name('addcategories.store');
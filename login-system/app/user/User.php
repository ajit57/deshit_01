<?php
namespace App\User;
use App\Database\Database as DB;
use PDO;


class User extends DB{
    public $email="";
    public $password="";
    public $id="";


    public function __construct(){
        parent::__construct();
    }

    public function setData($data=array()){
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('password',$data)){
            $this->password=md5($data['password']);
        }
        return $this;
    }

    public function store(){
        //var_dump($this->email);
        $query = "INSERT into users (email, password) Values(?, ?)";
        $data = array($this->email, $this->password);
        $sth = $this->db->prepare($query);
        $status = $sth->execute($data);
        if($status){
            echo "<script>alert('Registration successfull! You can login now');location.href='../login.php'</script>";
        }else{
            echo "<script>alert('There is a wrong with registration! please try again');location.href='../login.php'</script>";
        }
    }

    public function change_password(){
        $query="UPDATE `bdms_database`.`bdms_user` SET `password`=:password  WHERE `bdms_user`.`email` =:email";
        $result=$this->db->prepare($query);
        $result->execute(array($this->password));
        //$result->execute(array($this->password));
        //$result->execute(array(':password'=>$this->password,':email'=>$this->email));

        if($result){
            Message::message("
             <div class=\"alert alert-info\">
             <strong>Success!</strong> Password has been updated  successfully.
              </div>");
            //                                                                                                                  Utility::redirect('../../../../views/User/Profile/signUp.php');
        }
        else {
            echo "Error";
        }

    }

    public function view(){
        $query="SELECT * FROM `users` WHERE `users`.`email` =:email";
        $result=$this->db->prepare($query);
        $result->execute(array(':email'=>$this->email));
        $allData=$result->fetch(PDO::FETCH_OBJ);
        return $allData;
    }// end of view()


    

    public function update(){
            //Method need to be changed
        
    }

}


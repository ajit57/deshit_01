<?php
namespace App\Database;
use PDO;
use PDOException;

class Database 
{
	protected $db;
	protected $hostname = "localhost";
	protected $dbname = "crud-php-oopcu";
	protected $dbuser = "root";
	protected $dbpass = "";
	public function __construct()
	{
		try{
			$this->db = new PDO("mysql:hostname=$this->hostname;dbname=$this->dbname", $this->dbuser, $this->dbpass);
			//echo "Database connected....";
		}catch(PDOException $error){
			echo $error->getMessage();
		}
	}
}
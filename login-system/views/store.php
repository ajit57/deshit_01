<?php

require_once '../vendor/autoload.php';
use App\Student\Student;

$obj = new Student();
$strHobbies =  implode(", ", $_POST["hobbies"]);

$_POST["hobbies"] = $strHobbies;
$obj->setData($_POST);
$obj->store();

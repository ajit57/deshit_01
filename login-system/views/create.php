<?php
include 'header.php';
?>
	<section class="content-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2>Student List <a href="index.php" class="btn btn-primary pull-right">Back</a></h2>
					<form action="store.php" method="POST">
						<div class="form-group">
							<label for="name">Name : </label>
							<input type="text" name="name" id="name" class="form-control">
						</div>
						<div class="form-group">
							<label for="email">Email : </label>
							<input type="email" name="email" id="email" class="form-control">
						</div>
						<div class="form-group">
							<label for="birth_date">Date of Birth : </label>
							<input type="date" name="birth_date" id="birth_date" class="form-control">
						</div>
						<div class="form-group">
							<label for="gender">Gender : </label>
							<input type="radio" name="gender" value="male" checked> Male
							<input type="radio" name="gender" value="female"> Female
						</div>
						<div class="form-group">
							<label for="hobbies">Hobbies : </label>
							<input type="checkbox" name="hobbies[]" value="gardening"> Gardening
							<input type="checkbox" name="hobbies[]" value="swimming"> Swimming
							<input type="checkbox" name="hobbies[]" value="fishing"> Fishing
							<input type="checkbox" name="hobbies[]" value="traveling"> Traveling
						</div>
						<div class="form-group">
							<label for="address">Address : </label>
							<textarea name="address" class="form-control" id="" cols="30" rows="10"></textarea>
						</div>
						<button class="btn btn-primary" type="submit">Create</button>
					</form>
				</div>
			</div>
		</div>
	</section>

<?php
include 'footer.php';
?>
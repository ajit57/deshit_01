<?php
if(!isset($_SESSION) )session_start();
include_once('../../vendor/autoload.php');
use App\User\User;
use App\User\Auth;


$auth= new Auth();
$status= $auth->log_out();

session_destroy();
session_start();
echo "<script>alert('Logout successfully!');location.href='../login.php'</script>";

<?php
include 'header.php';
?>
	<section class="content-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<h2>Registration Form <a href="login.php" class="btn btn-primary pull-right">Already have an account Login</a></h2>
					<form action="profile/registration.php" method="POST">
						<div class="form-group">
							<label for="email">Email : </label>
							<input type="email" name="email" id="email" class="form-control">
						</div>
						<div class="form-group">
							<label for="password">Password : </label>
							<input type="password" name="password" id="password" class="form-control">
						</div>
						<button class="btn btn-primary" type="submit">Sign Up</button>
					</form>
				</div>
			</div>
		</div>
	</section>

<?php
include 'footer.php';
?>
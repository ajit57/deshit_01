<?php
if(!isset($_SESSION) )session_start();
include_once('../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$obj= new User();
$obj->setData($_SESSION);
$singleUser = $obj->view();

$auth= new Auth();
$status = $auth->setData($_SESSION)->logged_in();

if(!$status) {
    header("location:login.php");
}

include 'header.php';
use App\Student\Student;
$obj = new Student;
$alldata = $obj->index();

?>

<section class="main-content">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h4 class="text-right">Welcome <span style="color:yellow"><?php echo $_SESSION['email']; ?></span> <a class="btn btn-success" href="profile/logout.php">Logout</a></h4>
				<h2>Student List <a class="btn btn-primary pull-right" href="create.php">Create</a></h2>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>Sl.</th>
							<th>Name</th>
							<th>Email</th>
							<th>Birth Date</th>
							<th>Gender</th>
							<th>Hobby</th>
							<th>Address</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$i=1;
						foreach($alldata as $data){
						?>
						<tr>
							<td><?php echo $i++; ?></td>
							<td><?php echo $data->name; ?></td>
							<td><?php echo $data->email; ?></td>
							<td><?php echo $data->birth_date; ?></td>
							<td><?php echo $data->gender; ?></td>
							<td><?php echo $data->hobbies; ?></td>
							<td><?php echo $data->address; ?></td>
							<td>
								<a class="btn btn-warning btn-sm" href="edit.php?id=<?php echo $data->id; ?>">Edit</a>
								<a class="btn btn-danger btn-sm" onclick="return confirm('are you sure?')" href="delete.php?id=<?php echo $data->id; ?>">Delete</a>
							</td>
						</tr>

						<?php 
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

<?php
include 'footer.php';
?>
<?php

include "../vendor/autoload.php";
use App\Student\Student;

$obj = new Student;
$strhobbies = implode(',', $_POST['hobbies']);
$_POST['hobbies'] = $strhobbies;
$obj->setData($_POST);
$obj->update();
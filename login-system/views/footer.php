<footer class="footer text-center">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<p>All rights reserved by &copy; <a href="#">LICT</a></p>
					<p>Designed and Developed by &copy; <a href="#">CU MATH 2012-13</a></p>
				</div>
			</div>
		</div>
		
	</footer>
	<script src="../assets/js/jquery-3.2.1.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
</body>
</html>
<?php
include 'header.php';
?>
	<section class="content-area">
		<div class="container">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<h2>Login Form <a href="registration.php" class="btn btn-primary pull-right">Don't have an account Register</a></h2>
					<form action="profile/checklogin.php" method="POST">
						<div class="form-group">
							<label for="email">Email : </label>
							<input type="email" name="email" id="email" class="form-control">
						</div>
						<div class="form-group">
							<label for="password">Password : </label>
							<input type="password" name="password" id="password" class="form-control">
						</div>
						
						<button class="btn btn-primary" type="submit">Login</button>
					</form>
				</div>
			</div>
		</div>
	</section>

<?php
include 'footer.php';
?>